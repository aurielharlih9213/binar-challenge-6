import React, { Fragment } from "react";
import { Container } from "react-bootstrap";
import "./Homepage.scss";
import CarImage from "../../assets/img/img_car.png";
import { MainNavbar } from "../../assets/components/MainNavbar/MainNavbar";
import { Footer } from "../../assets/components/Footer/Footer";

const Homepage = () => {
  return (
    <Fragment>
      <MainNavbar />
      <div id="home">
        <Container>
          <div className="hero-section">
            <div className="section-1">
              <h3>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h3>
              <p>
                Selamat datang di Binar Car Rental. Kami menyediakan mobil
                kualitas terbaik dengan harga terjangkau. Selalu siap melayani
                kebutuhanmu untuk sewa mobil selama 24 jam.
              </p>
            </div>
            <div className="section-2">
              <img src={CarImage} alt="Car" />
            </div>
          </div>
        </Container>
      </div>
      <Footer />
    </Fragment>
  );
};

export default Homepage;
